import pytest
import uuid
import json
from app.app import get_app, get_store
from domain.todo import Todo
import app.todo # load http route definition

app = None

@pytest.fixture()
def app():
    app = get_app()
    # other setup can go here
    yield app
    # clean up / reset resources here

@pytest.fixture()
def store():
    store = get_store()
    yield store
    store.todo_list = {} # empty store

@pytest.fixture()
def client(app):
    print(app)
    return app.test_client()


@pytest.fixture()
def runner(app):
    return app.test_cli_runner()


def test_get_todo(client, store):
    # Given
    id = str(uuid.uuid4())
    content = "content"
    completed = False
    todo = Todo(id, content, completed)
    store.add_todo(todo)

    # When
    response = client.get("/api/todo")

    # Then
    assert response.status_code == 200
    assert response.json == [{ "id": id, "content": content, "completed": completed}]

def test_post_todo(client, store):
    # Given
    id = str(uuid.uuid4())
    content = "content"
    completed = False

    # When
    response = client.post("/api/todo", json={
        'id': id,
        'content': content,
        'completed': completed
    })

    # Then
    assert response.status_code == 201
    expected_new_todo = Todo(id, content, completed)
    assert expected_new_todo in store.get_todo()

if __name__ == '__main__':
    unittest.main()
