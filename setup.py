#!/usr/bin/env python
# -*- coding: UTF-8 -*-
import os
from setuptools import setup, find_packages

setup(
    name = 'todo_app',
    version='1.0',
    license='GNU General Public License v3',
    # author='Shalabh Aggarwal',
    # author_email='contact@shalabhaggarwal.com',
    description='Simple Flask api for managing todo list',
    packages=find_packages(where="todolist-api"),
    package_dir={"":"src"},
    platforms='any',
    install_requires=[
        'flask',
        'flask_cors'
    ],
    tests_require = [
        'pytest>=7'
    ],
    scripts=[
    
    ],
    test_suite="tests",
    zip_safe=False
)